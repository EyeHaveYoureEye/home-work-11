INSERT INTO Department(ID,Name) VALUES (DEFAULT,'Programming');
INSERT INTO Department(ID,Name) VALUES (DEFAULT,'Designers');
INSERT INTO Department(ID,Name) VALUES (DEFAULT,'Analysts');
INSERT INTO Department(ID,Name) VALUES (DEFAULT,'Managment');
INSERT INTO Department(ID,Name) VALUES (DEFAULT,'Accountant');
INSERT INTO Department(ID,Name) VALUES (DEFAULT,'Director');
INSERT INTO Department(ID,Name) VALUES (DEFAULT,'HR manager');
INSERT INTO Department(ID,Name) VALUES (DEFAULT,'Finance');

INSERT INTO Post(ID,Name) values (DEFAULT,'No post level');
INSERT INTO Post(ID,Name) values (DEFAULT,'Probation');
INSERT INTO Post(ID,Name) values (DEFAULT,'Junior');
INSERT INTO Post(ID,Name) values (DEFAULT,'Middle');
INSERT INTO Post(ID,Name) values (DEFAULT,'Senior');

INSERT INTO Customer(ID,first_name,last_name) values (DEFAULT,'Василиса','Серебрякова');
INSERT INTO Customer(ID,first_name,last_name) values (DEFAULT,'Есения','Трифонова');
INSERT INTO Customer(ID,first_name,last_name) values (DEFAULT,'Иван','Баженов');
INSERT INTO Customer(ID,first_name,last_name) values (DEFAULT,'София','Яковлева');
INSERT INTO Customer(ID,first_name,last_name) values (DEFAULT,'Владимир','Григорьев');

INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Ларионов','Семён',1,2,'2021-07-02',NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Кузьмин','Степан',2,1,'2020-01-31' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Серов','Илья',1,2,'2020-04-22' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Егорова','София',3,1,'2021-03-30' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Кузнецова','Алиса',1,3,'2020-04-04' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Титов','Егор',5,4,'2020-02-02' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Галкин','Артур',7,1,'2020-01-22' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Шарова','Таисия',8,1,'2020-09-05' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Кузьмина','Диана',2,1,'2020-11-12' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Киселева','Айлин',6,1,'2020-01-01' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Николаева','Ксения',4,1,'2021-04-01' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Горшкова','Вероника',1,4,'2021-01-03' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Александров','Игорь',1,5,'2021-07-21' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Фролова','Ангелина',2,1,'2021-03-05' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Данилов','Михаил',7,1,'2021-04-09' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Попов','Кирилл',1,3,'2021-02-09' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Харитонова','Аделина',1,4,'2021-01-03' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Лебедева','Вероника',1,3,'2021-07-04' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Еремина','Таисия',1,2,'2021-02-23' ,NULL);
INSERT INTO Staff(ID,first_name,last_name,department_id,post_id,date_of_employment,date_of_leave) values (DEFAULT,'Громов','Константин',1,3,'2021-02-23' ,NULL);

INSERT INTO Projects(ID,name,leader_id,date_of_start,date_of_end,customer_id) values (DEFAULT,'Fitness calendar', 1,'2021-07-02',NULL,1);
INSERT INTO Projects(ID,name,leader_id,date_of_start,date_of_end,customer_id) values (DEFAULT,'Smart calculator', 3,'2021-01-02',NULL,2);
INSERT INTO Projects(ID,name,leader_id,date_of_start,date_of_end,customer_id) values (DEFAULT,'Very smart calendar with calculator', 12,'2021-09-05',NULL,3);
INSERT INTO Projects(ID,name,leader_id,date_of_start,date_of_end,customer_id) values (DEFAULT,'Smartest calendalator', 13,'2021-10-01',NULL,4);
INSERT INTO Projects(ID,name,leader_id,date_of_start,date_of_end,customer_id) values (DEFAULT,'Pupa', 5,'2021-06-02',NULL,5);


INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,1,1,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,3,1,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,2,1,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,9,1,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,9,1,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,17,2,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,18,2,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,14,2,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,3,2,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,19,2,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,12,3,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,13,3,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,14,3,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,11,3,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,1,3,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,13,4,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,6,4,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,7,4,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,8,4,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,3,4,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,5,5,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,2,5,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,3,5,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,4,5,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,17,5,'2021-11-05',NULL);
INSERT INTO Staff_in_project(ID,staff_id,project_id,date_of_start,date_of_leave) values (DEFAULT,18,5,'2021-11-05',NULL);



