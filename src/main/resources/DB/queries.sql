-- список сотрудников, работающих над заданным проектом;
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
WHERE p.name = 'Very smart calendar with calculator'
ORDER BY sf.first_name;

-- список проектов, в которых участвует заданный сотрудник;
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
WHERE sf.id = 1
ORDER BY p.name;

SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
WHERE sf.first_name = 'Ларионов'
  AND sf.last_name = 'Семён'
ORDER BY p.name;

-- список сотрудников из заданного отдела, не участвующих ни в одном проекте;
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         LEFT JOIN Staff_in_project sfp
                   ON sf.ID = sfp.staff_id
         INNER JOIN Department d
                    ON d.ID = sf.department_id
         LEFT JOIN Projects p
                   ON p.ID = sfp.project_id
WHERE d.id = 1
  AND p.name IS NULL
ORDER BY sf.first_name;

SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         LEFT JOIN Staff_in_project sfp
                   ON sf.ID = sfp.staff_id
         INNER JOIN Department d
                    ON d.ID = sf.department_id
         LEFT JOIN Projects p
                   ON p.ID = sfp.project_id
WHERE d.name = 'Programming'
  AND p.name IS NULL
ORDER BY sf.first_name;

-- список сотрудников, не участвующих ни в одном проекте;
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         LEFT JOIN Staff_in_project sfp
                   ON sf.ID = sfp.staff_id
         LEFT JOIN Projects p
                   ON p.ID = sfp.project_id
WHERE p.name IS NULL
ORDER BY sf.first_name;

-- список подчиненных для заданного руководителя (по всем проектам, которыми он руководит);
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
                        AND sf.ID != p.leader_id
WHERE p.name IN (
    SELECT p.name
    FROM Staff sf
    INNER JOIN Staff_in_project sfp
    ON sf.ID = sfp.staff_id
    INNER JOIN Projects p
    ON p.ID = sfp.project_id
  AND sf.ID = p.leader_id
    WHERE sf.id = 12)
ORDER BY p.name;

SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
                        AND sf.ID != p.leader_id
WHERE p.name IN (
    SELECT p.name
    FROM Staff sf
    INNER JOIN Staff_in_project sfp
    ON sf.ID = sfp.staff_id
    INNER JOIN Projects p
    ON p.ID = sfp.project_id
  AND sf.ID = p.leader_id
    WHERE sf.first_name = 'Ларионов'
  AND sf.last_name = 'Семён')
ORDER BY p.name;

-- список руководителей для заданного сотрудника (по всем проектам, в которых он участвует);
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
                        AND sf.ID = p.leader_id
WHERE p.name IN (
    SELECT p.name
    FROM Staff sf
             INNER JOIN Staff_in_project sfp
                        ON sf.ID = sfp.staff_id
             INNER JOIN Projects p
                        ON p.ID = sfp.project_id
    WHERE sf.id = 3)
ORDER BY p.name;

SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
                        AND sf.ID = p.leader_id
WHERE p.name IN (
    SELECT p.name
    FROM Staff sf
             INNER JOIN Staff_in_project sfp
                        ON sf.ID = sfp.staff_id
             INNER JOIN Projects p
                        ON p.ID = sfp.project_id
    WHERE sf.first_name = 'Серов'
      AND sf.last_name = 'Илья')
ORDER BY p.name;

-- список сотрудников, участвующих в тех же проектах, что и заданный сотрудник;
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
WHERE p.name IN (
    SELECT p.name
    FROM Staff sf
             INNER JOIN Staff_in_project sfp
                        ON sf.ID = sfp.staff_id
             INNER JOIN Projects p
                        ON p.ID = sfp.project_id
    WHERE sf.first_name = 'Серов'
      AND sf.last_name = 'Илья')
  AND sf.first_name != 'Серов' AND sf.last_name != 'Илья'
ORDER BY p.name;

SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
WHERE p.name IN (
    SELECT p.name
    FROM Staff sf
             INNER JOIN Staff_in_project sfp
                        ON sf.ID = sfp.staff_id
             INNER JOIN Projects p
                        ON p.ID = sfp.project_id
    WHERE sf.id = 3)
  AND sf.id != 3
ORDER BY p.name;

-- список проектов, выполняемых для заданного заказчика;
SELECT сs.id, сs.first_name, сs.last_name, p.name
FROM Customer сs
         INNER JOIN Projects p
                    ON p.customer_id = сs.id
WHERE сs.id = 1
ORDER BY p.name;

SELECT сs.id, сs.first_name, сs.last_name, p.name
FROM Customer сs
         INNER JOIN Projects p
                    ON p.customer_id = сs.id
WHERE сs.first_name = 'Иван'
  AND last_name = 'Баженов'
ORDER BY p.name;

-- список сотрудников, участвующих в проектах, выполняемых для заданного заказчика;
SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
WHERE p.name IN (
    SELECT p.name
    FROM Customer сs
             INNER JOIN Projects p
                        ON p.customer_id = сs.id
    WHERE сs.id = 1)
ORDER BY p.name;

SELECT sf.id, sf.first_name, sf.last_name, p.name
FROM Staff sf
         INNER JOIN Staff_in_project sfp
                    ON sf.ID = sfp.staff_id
         INNER JOIN Projects p
                    ON p.ID = sfp.project_id
WHERE p.name IN (
    SELECT p.name
    FROM Customer сs
             INNER JOIN Projects p
                        ON p.customer_id = сs.id
    WHERE сs.first_name = 'Иван'
      AND last_name = 'Баженов')
ORDER BY p.name;

-- продумать, какие еще SQL запросы могут понадобиться отделу кадров в связи с введением “History”:

--Список активных проектов
SELECT id, name
FROM Projects
WHERE date_of_end is NULL;

--Список неактивных проектов
SELECT id, name
FROM Projects
WHERE date_of_end is NOT NULL;

--Список уволенных сотрудников
SELECT *
FROM Staff
WHERE date_of_leave is NOT NULL;

--Список активных сотрудников
SELECT *
FROM Staff
WHERE date_of_leave is NULL;














