package tech.pm.edu.hw9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HW9Boot {
  public static void main(String[] args) {
    SpringApplication.run(HW9Boot.class,args);
  }
}
