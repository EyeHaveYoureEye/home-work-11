package tech.pm.edu.hw9.domain.service.impl;

import org.springframework.stereotype.Service;
import tech.pm.edu.hw9.domain.model.Customer;
import tech.pm.edu.hw9.domain.repository.CustomerRepository;
import tech.pm.edu.hw9.domain.service.CustomerService;
import tech.pm.edu.hw9.web.ExceptionHandler.GenericException;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

  public CustomerServiceImpl(CustomerRepository customerRepository) {
    this.customerRepository = customerRepository;
  }

  CustomerRepository customerRepository;

  @Override
  public List<Customer> getAllById(String id) {
    Integer parsedId = Integer.parseInt(id);
    List<Customer> list = customerRepository.findAllById(parsedId);
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Customer> getAll() {
    List<Customer> list = customerRepository.findAll();
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  @Transactional
  public void delete(String customerId){
    try {
      customerRepository.deleteById(Integer.parseInt(customerId));
    }catch (Exception e){
      throw new GenericException("Delete error");
    }
  }

  @Override
  @Transactional
  public Customer insert(Customer customer){
    try {
      return customerRepository.save(customer);
    }catch (Exception e){
      throw new GenericException("Insert error");
    }
  }

}
