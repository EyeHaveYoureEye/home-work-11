package tech.pm.edu.hw9.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.pm.edu.hw9.domain.model.Post;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Integer> {
  List<Post> findAllById(Integer id);
}
