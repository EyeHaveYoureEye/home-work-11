package tech.pm.edu.hw9.domain.service.impl;

import org.springframework.stereotype.Service;
import tech.pm.edu.hw9.domain.model.Post;
import tech.pm.edu.hw9.domain.repository.PostRepository;
import tech.pm.edu.hw9.domain.service.PostService;
import tech.pm.edu.hw9.web.ExceptionHandler.GenericException;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

  public PostServiceImpl(PostRepository postRepository) {
    this.postRepository = postRepository;
  }

  PostRepository postRepository;

  @Override
  public List<Post> getAllById(String id) {
    Integer parsedId = Integer.parseInt(id);
    List<Post> list = postRepository.findAllById(parsedId);
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Post> getAll() {
    List<Post> list = postRepository.findAll();
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  @Transactional
  public void delete(String postId) {
    try {
      postRepository.deleteById(Integer.parseInt(postId));
    }catch (Exception e){
      throw new GenericException("Delete error");
    }
  }

  @Override
  @Transactional
  public Post insert(Post post) {
    try {
      return postRepository.save(post);
    }catch (Exception e){
      throw new GenericException("Insert error");
    }
  }

}
