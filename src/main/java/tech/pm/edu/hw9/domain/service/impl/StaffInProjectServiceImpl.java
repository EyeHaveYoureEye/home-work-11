package tech.pm.edu.hw9.domain.service.impl;

import org.springframework.stereotype.Service;
import tech.pm.edu.hw9.domain.model.StaffInProject;
import tech.pm.edu.hw9.domain.repository.StaffInProjectRepository;
import tech.pm.edu.hw9.domain.service.StaffInProjectService;
import tech.pm.edu.hw9.web.ExceptionHandler.GenericException;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class StaffInProjectServiceImpl implements StaffInProjectService {

  public StaffInProjectServiceImpl(StaffInProjectRepository staffInProjectRepository) {
    this.staffInProjectRepository = staffInProjectRepository;
  }

  StaffInProjectRepository staffInProjectRepository;

  @Override
  public List<StaffInProject> getAll() {
    List<StaffInProject> list = staffInProjectRepository.findAll();
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  @Transactional
  public void delete(String staffInProjectId) {
    try {
      staffInProjectRepository.deleteById(Integer.parseInt(staffInProjectId));
    } catch (Exception e) {
      throw new GenericException("Delete error");
    }
  }

  @Override
  @Transactional
  public StaffInProject insert(StaffInProject staffInProject) {
    try {
      return staffInProjectRepository.save(staffInProject);
    } catch (Exception e) {
      throw new GenericException("Insert error");
    }
  }
}
