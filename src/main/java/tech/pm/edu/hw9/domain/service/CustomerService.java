package tech.pm.edu.hw9.domain.service;

import tech.pm.edu.hw9.domain.model.Customer;

import java.util.List;

public interface CustomerService {
  List<Customer> getAllById(String id);
  List<Customer> getAll();
  void delete(String customerId);
  Customer insert(Customer customer);
}
