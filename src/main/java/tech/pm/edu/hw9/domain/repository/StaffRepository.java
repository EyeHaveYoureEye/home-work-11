package tech.pm.edu.hw9.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.pm.edu.hw9.domain.model.Staff;

import java.util.List;

@Repository
public interface StaffRepository extends JpaRepository<Staff,Integer> {

  List<Staff> findAllById(Integer id);

  @Query(value = "SELECT *" +
          "FROM Staff sf" +
          "INNER JOIN Staff_in_project sfp" +
          "ON sf.ID = sfp.staff_id" +
          "INNER JOIN Projects p" +
          "ON p.ID = sfp.project_id" +
          "WHERE p.id = :projectId" +
          "ORDER BY sf.first_name",nativeQuery = true)
  List<Staff> findAllByProject(Integer projectId);

  @Query(value = "SELECT *" +
          "FROM Staff sf" +
          "LEFT JOIN Staff_in_project sfp" +
          "ON sf.ID = sfp.staff_id" +
          "INNER JOIN Department d" +
          "ON d.ID = sf.department_id" +
          "LEFT JOIN Projects p" +
          "ON p.ID = sfp.project_id" +
          "WHERE d.id = :departmentId" +
          "AND p.name IS NULL" +
          "ORDER BY sf.first_name",nativeQuery = true)
  List<Staff> findAllByDepartmentAndProjectNull(Integer departmentId);

  @Query(value = "SELECT * " +
          "FROM Staff sf " +
          "LEFT JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "LEFT JOIN Projects p " +
          "ON p.ID = sfp.project_id" +
          " WHERE p.name IS NULL " +
          "ORDER BY sf.first_name",nativeQuery = true)
  List<Staff> findAllByProjectNull();

  @Query(value = "SELECT * " +
          "FROM Staff sf " +
          "INNER JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "INNER JOIN Projects p " +
          "ON p.ID = sfp.project_id " +
          "AND sf.ID != p.leader_id " +
          "WHERE p.name " +
          "IN ( SELECT p.name " +
          "FROM Staff sf " +
          "INNER JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "INNER JOIN Projects p " +
          "ON p.ID = sfp.project_id " +
          "AND sf.ID = p.leader_id " +
          "WHERE sf.id = :staffId) " +
          "ORDER BY p.name",nativeQuery = true)
  List<Staff> findAllByStaff(Integer staffId);

  @Query(value = "SELECT * " +
          "FROM Staff sf " +
          "INNER JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "INNER JOIN Projects p " +
          "ON p.ID = sfp.project_id " +
          "AND sf.ID = p.leader_id " +
          "WHERE p.name " +
          "IN ( SELECT p.name " +
          "FROM Staff sf " +
          "INNER JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "INNER JOIN Projects p " +
          "ON p.ID = sfp.project_id " +
          "WHERE sf.id = :leadId) " +
          "ORDER BY p.name",nativeQuery = true)
  List<Staff> findLeadByStaff(Integer leadId);

  @Query(value = "SELECT * " +
          "FROM Staff sf " +
          "INNER JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "INNER JOIN Projects p " +
          "ON p.ID = sfp.project_id " +
          "WHERE p.name " +
          "IN ( SELECT p.name " +
          "FROM Staff sf " +
          "INNER JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "INNER JOIN Projects p " +
          "ON p.ID = sfp.project_id " +
          "WHERE sf.id = :staffId) " +
          "AND sf.id != :staffId " +
          "ORDER BY p.name",nativeQuery = true)
  List<Staff> findAllBySameProject(Integer staffId);

  @Query(value = "SELECT * " +
          "FROM Staff sf " +
          "INNER JOIN Staff_in_project sfp " +
          "ON sf.ID = sfp.staff_id " +
          "INNER JOIN Projects p " +
          "ON p.ID = sfp.project_id " +
          "WHERE p.name " +
          "IN ( SELECT p.name " +
          "FROM Customer сs " +
          "INNER JOIN Projects p " +
          "ON p.customer_id = сs.id " +
          "WHERE сs.id = :customerId) " +
          "ORDER BY p.name",nativeQuery = true)
  List<Staff> findAllByCustomerProject(Integer customerId);


}
