package tech.pm.edu.hw9.domain.service;

import tech.pm.edu.hw9.domain.model.Post;

import java.util.List;

public interface PostService {
  List<Post> getAllById(String id);
  List<Post> getAll();
  void delete(String postId);
  Post insert(Post post);
}
