package tech.pm.edu.hw9.domain.service;

import tech.pm.edu.hw9.domain.model.Projects;

import java.util.List;

public interface ProjectsService  {

  List<Projects> getAllById(String id);
  List<Projects> getProjectsByStaff(String staffId);
  List<Projects> getProjectsByCustomer(String customerId);
  List<Projects> getAll();
  void  delete(String projectsId);
  Projects insert(Projects projects);


}
