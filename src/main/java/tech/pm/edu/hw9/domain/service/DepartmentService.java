package tech.pm.edu.hw9.domain.service;

import tech.pm.edu.hw9.domain.model.Department;

import java.util.List;

public interface DepartmentService {
  List<Department> getAllById(String id);
  List<Department> getAll();
  void  delete(String departmentId);
  Department insert(Department department);
}
