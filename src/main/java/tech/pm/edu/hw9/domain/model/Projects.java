package tech.pm.edu.hw9.domain.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "projects")
@Table(name = "projects")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "relationClass", "staffInProjectSet"})
public class Projects {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column
  private String name;
  @Column
  private String date_of_start;
  @Column
  private String date_of_end;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.DETACH,CascadeType.REFRESH})
  @JoinColumn(name = "customer_id")
  private Customer customer;

  @JsonManagedReference
  @OneToMany(mappedBy = "projects",fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH}, orphanRemoval = true)
  private Set<StaffInProject> staffInProjectSet;


}
