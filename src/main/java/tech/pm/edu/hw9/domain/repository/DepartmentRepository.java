package tech.pm.edu.hw9.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.pm.edu.hw9.domain.model.Department;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department,Integer> {
  List<Department> findAllById(Integer id);
}
