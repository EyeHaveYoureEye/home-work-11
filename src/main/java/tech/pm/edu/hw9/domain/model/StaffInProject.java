package tech.pm.edu.hw9.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "staff_in_project")
@Table(name = "staff_in_project")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "relationClass",})
public class StaffInProject {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column
  private String date_of_start;
  @Column
  private String date_of_leave;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.DETACH,CascadeType.REFRESH})
  @JoinColumn(name = "project_id")
  private Projects projects;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.DETACH,CascadeType.REFRESH})
  @JoinColumn(name = "staff_id")
  private Staff staff;
}
