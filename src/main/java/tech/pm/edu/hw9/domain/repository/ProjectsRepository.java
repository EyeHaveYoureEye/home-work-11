package tech.pm.edu.hw9.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.pm.edu.hw9.domain.model.Projects;

import java.util.List;

@Repository
public interface ProjectsRepository extends JpaRepository<Projects,Integer> {

  List<Projects> findAllById(Integer id);

  @Query(value = "SELECT *" +
          "FROM Staff sf" +
          "INNER JOIN Staff_in_project sfp\n" +
          "ON sf.ID = sfp.staff_id\n" +
          "INNER JOIN Projects p\n" +
          "ON p.ID = sfp.project_id\n" +
          "WHERE sf.id = :staffId" +
          "ORDER BY p.name",nativeQuery = true)
 List<Projects> findProjectsByStaff(Integer staffId);

  @Query(value = "SELECT * " +
          "FROM Customer сs " +
          "INNER JOIN Projects p " +
          "ON p.customer_id = сs.id " +
          "WHERE сs.id = :customerId " +
          "ORDER BY p.name",nativeQuery = true)
  List<Projects> findProjectsByCustomer(Integer customerId);


}
