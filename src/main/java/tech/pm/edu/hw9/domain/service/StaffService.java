package tech.pm.edu.hw9.domain.service;

import tech.pm.edu.hw9.domain.model.Staff;

import java.util.List;

public interface StaffService {

  List<Staff> getAllById(String Id);
  List<Staff> getAllByProject(String projectId);
  List<Staff> getAllByDepartmentAndProjectNull(String departmentId);
  List<Staff> getAllByProjectNull();
  List<Staff> getAllByStaff(String staffId);
  List<Staff> getLeadByStaff(String leadId);
  List<Staff> getAllBySameProject(String staffId);
  List<Staff> getAllByCustomerProject(String customerId);
  Staff insert(Staff staff);
  List<Staff> getAll();
  void  delete(String customerId);

}
