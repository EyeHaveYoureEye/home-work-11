package tech.pm.edu.hw9.domain.service.impl;

import org.springframework.stereotype.Service;
import tech.pm.edu.hw9.domain.model.Department;
import tech.pm.edu.hw9.domain.repository.DepartmentRepository;
import tech.pm.edu.hw9.domain.service.DepartmentService;
import tech.pm.edu.hw9.web.ExceptionHandler.GenericException;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

  public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
    this.departmentRepository = departmentRepository;
  }

  DepartmentRepository departmentRepository;

  @Override
  public List<Department> getAllById(String id) {
    Integer parsedId = Integer.parseInt(id);
    List<Department> list = departmentRepository.findAllById(parsedId);
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Department> getAll() {
    List<Department> list = departmentRepository.findAll();
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  @Transactional
  public void delete(String departmentId) {
    try {
      departmentRepository.deleteById(Integer.parseInt(departmentId));
    }catch (Exception e){
      throw new GenericException("delete error");
    }

  }

  @Override
  @Transactional
  public Department insert(Department department) {
    try {
      return  departmentRepository.save(department);
    }catch (Exception e){
      throw new GenericException("Save error");
    }

  }
}
