package tech.pm.edu.hw9.domain.service.impl;

import org.springframework.stereotype.Service;
import tech.pm.edu.hw9.domain.model.Staff;
import tech.pm.edu.hw9.domain.repository.StaffRepository;
import tech.pm.edu.hw9.domain.service.StaffService;
import tech.pm.edu.hw9.web.ExceptionHandler.GenericException;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {

  StaffRepository staffRepository;

  public StaffServiceImpl(StaffRepository staffRepository) {
    this.staffRepository = staffRepository;
  }

  @Override
  public List<Staff> getAllById(String Id) {
    Integer parsedId = Integer.parseInt(Id);
    List<Staff> list = staffRepository.findAllById(parsedId);
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Staff> getAllByProject(String projectId) {
    Integer parsedProjectId = Integer.parseInt(projectId);
    List<Staff> list = staffRepository.findAllByProject(parsedProjectId);
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Staff> getAllByDepartmentAndProjectNull(String departmentId) {
    Integer parsedDepartmentId = Integer.parseInt(departmentId);
    List<Staff> list = staffRepository.findAllByDepartmentAndProjectNull(parsedDepartmentId);
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Staff> getAllByProjectNull() {
    List<Staff> list = staffRepository.findAllByProjectNull();
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Staff> getAllByStaff(String staffId) {
    Integer parsedStaffId = Integer.parseInt(staffId);
    List<Staff> list = staffRepository.findAllByStaff(parsedStaffId);
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Staff> getLeadByStaff(String leadId) {
    Integer parsedLeadId = Integer.parseInt(leadId);
    List<Staff> list = staffRepository.findLeadByStaff(parsedLeadId);
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Staff> getAllBySameProject(String staffId) {
    Integer parsedStaffId = Integer.parseInt(staffId);
    List<Staff> list = staffRepository.findAllBySameProject(parsedStaffId);
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Staff> getAllByCustomerProject(String customerId) {
    Integer parsedCustomerId = Integer.parseInt(customerId);
    List<Staff> list = staffRepository.findAllByCustomerProject(parsedCustomerId);
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }


  @Override
  @Transactional
  public Staff insert(Staff staff) {
    try {
      return staffRepository.save(staff);
    } catch (Exception e) {
      throw new GenericException("Insert error");
    }
  }

  @Override
  public List<Staff> getAll() {
    List<Staff> list = staffRepository.findAll();
    if (list.isEmpty()) {
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  @Transactional
  public void delete(String customerId) {
    try {
      staffRepository.deleteById(Integer.parseInt(customerId));
    }catch (Exception e){
      throw new GenericException("Delete error");
    }
  }
}
