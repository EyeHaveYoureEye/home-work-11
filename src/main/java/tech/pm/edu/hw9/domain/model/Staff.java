package tech.pm.edu.hw9.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "staff")
@Table(name = "staff")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "relationClass", "staffInProjectSet"})
public class Staff {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column
  private String first_name;
  @Column
  private String last_name;
  @Column
  private String date_of_employment;
  @Column
  private String date_of_leave;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.DETACH,CascadeType.REFRESH})
  @JoinColumn(name = "department_id")
  private Department department;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.DETACH,CascadeType.REFRESH})
  @JoinColumn(name = "post_id")
  private Post post;

  @JsonIgnore
  @JsonManagedReference
  @OneToMany(mappedBy = "staff",fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH}, orphanRemoval = true)
  private Set<StaffInProject> staffInProjectSet;
}
