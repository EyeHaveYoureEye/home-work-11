package tech.pm.edu.hw9.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.pm.edu.hw9.domain.model.StaffInProject;

@Repository
public interface StaffInProjectRepository extends JpaRepository<StaffInProject,Integer> {
}
