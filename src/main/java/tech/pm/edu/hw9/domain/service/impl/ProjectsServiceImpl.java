package tech.pm.edu.hw9.domain.service.impl;

import org.springframework.stereotype.Service;
import tech.pm.edu.hw9.domain.model.Projects;
import tech.pm.edu.hw9.domain.repository.ProjectsRepository;
import tech.pm.edu.hw9.domain.service.ProjectsService;
import tech.pm.edu.hw9.web.ExceptionHandler.GenericException;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectsServiceImpl implements ProjectsService {
  public ProjectsServiceImpl(ProjectsRepository projectsRepository) {
    this.projectsRepository = projectsRepository;
  }

  ProjectsRepository projectsRepository;

  @Override
  public List<Projects> getAllById(String id) {
    Integer parsedId = Integer.parseInt(id);
    List<Projects> list = projectsRepository.findAllById(parsedId);
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Projects> getProjectsByStaff(String staffId) {
    Integer parsedStaffId = Integer.parseInt(staffId);
    List<Projects> list = projectsRepository.findProjectsByStaff(parsedStaffId);
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Projects> getProjectsByCustomer(String customerId) {
    Integer parsedCustomerId = Integer.parseInt(customerId);
    List<Projects> list = projectsRepository.findProjectsByCustomer(parsedCustomerId);
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  public List<Projects> getAll() {
    List<Projects> list = projectsRepository.findAll();
    if (list.isEmpty()){
      throw new GenericException("List empty " + list.getClass());
    }
    return list;
  }

  @Override
  @Transactional
  public void delete(String projectsId) {
    try {
      projectsRepository.deleteById(Integer.parseInt(projectsId));
    }catch (Exception e){
      throw new GenericException("Delete error");
    }
  }

  @Override
  @Transactional
  public Projects insert(Projects projects) {
    try {
      return projectsRepository.save(projects);
    }catch (Exception e){
      throw new GenericException("Insert error");
    }
  }
}
