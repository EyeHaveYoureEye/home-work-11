package tech.pm.edu.hw9.domain.service;

import tech.pm.edu.hw9.domain.model.StaffInProject;

import java.util.List;

public interface StaffInProjectService {
  List<StaffInProject> getAll();

  void delete(String staffInProjectId);

  StaffInProject insert(StaffInProject staffInProject);
}
