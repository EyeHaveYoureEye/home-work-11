package tech.pm.edu.hw9.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tech.pm.edu.hw9.domain.model.StaffInProject;
import tech.pm.edu.hw9.domain.service.StaffInProjectService;

import java.util.List;

@Controller
@RequestMapping("/staffinproject")
public class StaffInProjectController {

  private final StaffInProjectService staffInProjectService;

  public StaffInProjectController(StaffInProjectService staffInProject) {
    this.staffInProjectService = staffInProject;
  }


  @GetMapping(path = "/all")
  @ResponseBody
  public List<StaffInProject> getAll(){
    return staffInProjectService.getAll();
  }

  @DeleteMapping(path = "/delete/{deleteId}")
  @ResponseBody
  public void  deleteById(@PathVariable(name = "deleteId") String deleteId){
    staffInProjectService.delete(deleteId);
  }

  @PostMapping(path = "/insert")
  @ResponseBody
  public StaffInProject insert(@RequestBody StaffInProject staffInProject){
    return staffInProjectService.insert(staffInProject);
  }
}
