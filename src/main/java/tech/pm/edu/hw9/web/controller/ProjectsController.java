package tech.pm.edu.hw9.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tech.pm.edu.hw9.domain.model.Projects;
import tech.pm.edu.hw9.domain.service.ProjectsService;

import java.util.List;

@Controller
@RequestMapping("/projects")
public class ProjectsController {

  private final ProjectsService projectsService;

  public ProjectsController(ProjectsService projectsService) {
    this.projectsService = projectsService;
  }


  @GetMapping(path = "/all")
  @ResponseBody
  public List<Projects> getAll(){
    return projectsService.getAll();
  }

  @GetMapping(path = "/{projectsId}")
  @ResponseBody
  public List<Projects> getById(@PathVariable(name = "projectsId") String projectsId){
    return  projectsService.getAllById(projectsId);
  }

  @DeleteMapping(path = "/delete/{deleteId}")
  @ResponseBody
  public void  deleteById(@PathVariable(name = "deleteId") String deleteId){
    projectsService.delete(deleteId);
  }

  @PostMapping(path = "/insert")
  @ResponseBody
  public Projects insert(@RequestBody Projects projects){
    return projectsService.insert(projects);
  }

  @GetMapping(name = "/byStaffId/{staffId}")
  @ResponseBody
  public List<Projects> getProjectsByStaff(@PathVariable(name = "staffId") String staffId){
    return projectsService.getProjectsByStaff(staffId);
  }

  @GetMapping(path = "/ByCustomerId/{customerId}")
  @ResponseBody
  public List<Projects> getProjectsByCustomer(@PathVariable(name = "customerId") String customerId){
    return projectsService.getProjectsByCustomer(customerId);
  }
}
