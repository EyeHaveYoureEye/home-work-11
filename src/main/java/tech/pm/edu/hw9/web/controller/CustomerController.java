package tech.pm.edu.hw9.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tech.pm.edu.hw9.domain.model.Customer;
import tech.pm.edu.hw9.domain.service.CustomerService;

import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {

  private final CustomerService customerService;

  public CustomerController(CustomerService customer) {
    this.customerService = customer;
  }

  @GetMapping(path = "/all")
  @ResponseBody
  public List<Customer> getAll(){
    return customerService.getAll();
  }

  @GetMapping(path = "/{customerId}")
  @ResponseBody
  public List<Customer> getById(@PathVariable(name = "customerId") String customerId){
    return customerService.getAllById(customerId);
  }

  @DeleteMapping(path = "/delete/{deleteId}")
  @ResponseBody
  public void  deleteById(@PathVariable(name = "deleteId") String deleteId){
    customerService.delete(deleteId);
  }

  @PostMapping(path = "/insert")
  @ResponseBody
  public Customer insert(@RequestBody Customer customer){
    return customerService.insert(customer);
  }

}