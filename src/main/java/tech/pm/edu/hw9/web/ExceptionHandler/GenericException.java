package tech.pm.edu.hw9.web.ExceptionHandler;

public class GenericException extends RuntimeException {
  public GenericException(String message) {
    super(message);
  }
}
