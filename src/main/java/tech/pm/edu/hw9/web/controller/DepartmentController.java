package tech.pm.edu.hw9.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tech.pm.edu.hw9.domain.model.Department;
import tech.pm.edu.hw9.domain.service.DepartmentService;

import java.util.List;

@Controller
@RequestMapping("/department")
public class DepartmentController {

  private final DepartmentService departmentService;

  public DepartmentController(DepartmentService departmentService) {
    this.departmentService = departmentService;
  }

  @GetMapping(path = "/all")
  @ResponseBody
  public List<Department> getAll(){
    return departmentService.getAll();
  }

  @GetMapping(path = "/{departmentId}")
  @ResponseBody
  public List<Department> getById(@PathVariable(name = "departmentId") String departmentId){
    return  departmentService.getAllById(departmentId);
  }

  @DeleteMapping(path = "/delete/{deleteId}")
  @ResponseBody
  public void  deleteById(@PathVariable(name = "deleteId") String deleteId){
    departmentService.delete(deleteId);
  }

  @PostMapping(path = "/insert")
  @ResponseBody
  public Department insert(@RequestBody Department department){
    return departmentService.insert(department);
  }
}
