package tech.pm.edu.hw9.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tech.pm.edu.hw9.domain.model.Post;
import tech.pm.edu.hw9.domain.service.PostService;

import java.util.List;

@Controller
@RequestMapping("/post")
public class PostController {

  private final PostService postService;


  public PostController(PostService postService) {
    this.postService = postService;
  }

  @GetMapping(path = "/all")
  @ResponseBody
  public List<Post> getAll(){
    return postService.getAll();
  }

  @GetMapping(path = "/{postId}")
  @ResponseBody
  public List<Post> getById(@PathVariable(name = "postId") String postId){
    return  postService.getAllById(postId);
  }

  @DeleteMapping(path = "/delete/{deleteId}")
  @ResponseBody
  public void  deleteById(@PathVariable(name = "deleteId") String deleteId){
    postService.delete(deleteId);
  }

  @PostMapping(path = "/insert")
  @ResponseBody
  public Post insert(@RequestBody Post post){
    return postService.insert(post);
  }
}
