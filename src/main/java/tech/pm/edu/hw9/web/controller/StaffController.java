package tech.pm.edu.hw9.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tech.pm.edu.hw9.domain.model.Staff;
import tech.pm.edu.hw9.domain.service.StaffService;

import java.util.List;

@Controller
@RequestMapping("/staff")
public class StaffController {
  private final StaffService staffService;


  public StaffController(StaffService staffService) {
    this.staffService = staffService;
  }

  @GetMapping(path = "/all")
  @ResponseBody
  public List<Staff> getAll(){
    return staffService.getAll();
  }

  @GetMapping(path = "/{staffId}")
  @ResponseBody
  public List<Staff> getById(@PathVariable(name = "staffId") String staffId){
    return  staffService.getAllById(staffId);
  }

  @DeleteMapping(path = "/delete/{deleteId}")
  @ResponseBody
  public void  deleteById(@PathVariable(name = "deleteId") String deleteId){
    staffService.delete(deleteId);
  }

  @PostMapping(path = "/insert")
  @ResponseBody
  public Staff insert(@RequestBody Staff department){
    return staffService.insert(department);
  }

  @GetMapping(path = "/ByProject/{projectId}")
  @ResponseBody
  public List<Staff> getAllByProject(@PathVariable(name = "projectId") String projectId){
    return staffService.getAllByProject(projectId);
  }

  @GetMapping(path = "/ByDepartmentAndProjectNull/{departmentId}")
  @ResponseBody
  public List<Staff> getAllByDepartmentAndProjectNull(@PathVariable(name = "departmentId")String departmentId){
    return staffService.getAllByDepartmentAndProjectNull(departmentId);
  }

  @GetMapping(path = "/ByProjectNull")
  @ResponseBody
  public List<Staff> getAllByProjectNull(){
    return staffService.getAllByProjectNull();
  }

  @GetMapping(path = "/ByStaff/{staffId}")
  @ResponseBody
  public List<Staff> getAllByStaff(@PathVariable(name = "staffId")String staffId){
    return staffService.getAllByStaff(staffId);
  }

  @GetMapping(path = "/LeadByStaff/{leadId}")
  @ResponseBody
  public List<Staff> getLeadByStaff(@PathVariable(name = "leadId")String leadId){
    return staffService.getLeadByStaff(leadId);
  }

  @GetMapping(path = "/BySameProject/{staffId}")
  @ResponseBody
  public List<Staff> getAllBySameProject(@PathVariable(name = "staffId")String staffId){
    return staffService.getAllBySameProject(staffId);
  }


  @GetMapping(path = "/ByCustomerProject/{customerId}")
  @ResponseBody
  public List<Staff> getAllByCustomerProject(@PathVariable(name = "customerId")String customerId){
    return staffService.getAllByCustomerProject(customerId);
  }


}
